#include "Mesh.h"

Mesh::Mesh(){

}

Mesh::Mesh(int inputCount, int outputCount){
    inputs.resize(inputCount);
    outputs.resize(outputCount);
}

bool Mesh::getVal(){
    return value;
}

void Mesh::updateVal(){
    bool result = false;
    for(auto input: inputs){
       result |= input->getValue();
    }

    value = result;
}

vector<Circuit*> Mesh::getOutputs(){
    return outputs;
}

void Mesh::connectInput(Connection* inputConnection){
    inputs.push_back(inputConnection);
}

void Mesh::connectOutput(Circuit* outputCircuit){
    outputs.push_back(outputCircuit);
}