#ifndef MESH
#define MESH

#include <vector>
#include "Circuit.h"
#include "Connection.h"

using std::vector;

class Circuit;

class Mesh{
private:
    bool value;
    vector<Connection*> inputs;
    vector<Circuit*> outputs;

public:
    Mesh();
    Mesh(int inputCount, int outputCount);

    vector<Circuit*> getOutputs();

    bool getVal();
    void updateVal();

    void connectInput(Connection* inputConnection);
    void connectOutput(Circuit* outputCircuit);
};

#include "Mesh.hpp"

#endif