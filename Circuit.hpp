#include "Circuit.h"

string getline(string &input){
    string result;

    if(input.find('\n') != string::npos){
        result = input.substr(0, input.find('\n'));
        input.erase(0,input.find('\n') + 1);
    } else {
        result = input;
    }

    return result;
}

vector<string> tokenize(string input, char c = ','){
    int index = 0;
    vector<string> output;
    while(input.find(c) != string::npos){
        index = input.find(c);
        output.push_back(input.substr(0,index));
        input = input.substr(index + 1);
    }
    output.push_back(input);
    return output;
}

Circuit::Circuit(){

}

Circuit::~Circuit(){
    for(auto output: outputs){
        delete output;
    }
    
    for(auto input: inputs){
        delete input;
    }

    for(auto entry: subCircuits){
        delete entry.second;
    }

    for(auto mesh: meshes){
        delete mesh;
    }
}

Circuit::Circuit(int inputCount, int outputCount){
    inputs.resize(inputCount);
    outputs.resize(outputCount);
}

void Circuit::update(){
    set<Circuit*> nextUpdateList;
    set<Mesh*> meshUpdateList;

    //Run through every component on the update queue
    for(auto activeCircuit: updateList){
        //Call update
        activeCircuit->update();

        //Run through every connected mesh 
        for(Connection* connection: activeCircuit->outputs){
            //TODO: Remove override here
            if(connection->valueChanged() || true){
                Mesh* connectedMesh = connection->getTiePoint();
                    
                //put each connected mesh on the meshqueue, if it's not already on the queue
                meshUpdateList.insert(connectedMesh);
                    
                //get all affected components and put them on the new update queue, if they're not already on the queue
                vector<Circuit*> affectedCircuits = connectedMesh->getOutputs();
                nextUpdateList.insert(affectedCircuits.begin(),affectedCircuits.end());
            }
        }
    }

    //update the value of every mesh
    for(auto mesh: meshUpdateList){
        mesh->updateVal();
    }

    //set the next update queue
    updateList = nextUpdateList;
}

void Circuit::setPosition(int x, int y){
    position = pair<int,int>(x,y);
}

void Circuit::setRotation(Rotation r){
    rotation = r;
}

void Circuit::connectInputMesh(Mesh* inputMesh, int index){
    inputs[index] = inputMesh;
    inputMesh->connectOutput(this);
}

void Circuit::connectOutputMesh(Mesh* outputMesh, int index){
    outputs[index] = new Connection(outputMesh);
    outputMesh->connectInput(outputs[index]);
}

void Circuit::addSubCircuit(Circuit* subCircuit, string name){
    if(subCircuits.find(name) == subCircuits.end()){
        subCircuits.insert(pair<string,Circuit*>(name,subCircuit));
    }
}

Circuit* Circuit::getSubCircuit(string name){
    if(subCircuits.find(name) != subCircuits.end()){
        return subCircuits.at(name);
    }
}

void Circuit::insertToUpdateList(Circuit* toUpdate){
    updateList.insert(toUpdate);
}

bool Circuit::isStable(){
    return updateList.empty();
}

void Circuit::parse(string text){
    string line = getline(text);

    map<string, Mesh*> meshMap;
    vector<string> tokens;
    vector<string> inputs;
    vector<string> outputs;

    while(line != "eoc"){
        tokens = tokenize(line);
        inputs = tokenize(tokens[5], '.');
        outputs = tokenize(tokens[6], '.');
        
        Circuit* subCircuit = ComponentFactory::get().getGeneratorMap().at(tokens[0])();

        subCircuit->setPosition(stoi(tokens[2]), stoi(tokens[3]));
        subCircuit->setRotation((Rotation)stoi(tokens[4]));


        Mesh* m;
        for(int i = 0; (i < inputs.size()) && (inputs[0] != ""); i++){
            if(!meshMap.count(inputs[i])){
                m = new Mesh();
                meshMap.emplace(inputs[i], m);
            } else {
                m = meshMap.at(inputs[i]);
            }
            subCircuit->connectInputMesh(m, i);
        }

        for(int i = 0; (i < outputs.size()) && (outputs[0] != ""); i++){
            if(!meshMap.count(outputs[i])){
                m = new Mesh();
                meshMap.emplace(outputs[i], m);
            } else {
                m = meshMap.at(outputs[i]);
            }
            subCircuit->connectOutputMesh(m, i);
        }

        // Check if the circuit type is of abstract circuit
        if(tokens[0] == Circuit::type()){
            // If the circuit is an abstract circuit, recursively parse 
            subCircuit->parse(text);
        } else {
            // Otherwise, parse the extra data 
            subCircuit->parse(tokens[7]);
        }

        this->addSubCircuit(subCircuit, tokens[1]);
        line = getline(text);
    }
}

string Circuit::type(){
    return "Abstract Circuit";
}