#ifndef OUTPUT
#define OUTPUT

#include "../Circuit.h"

class Output: public Circuit{
public:
    Output(){
        inputs.resize(1);
        outputs.resize(1);
    }

    Output(Connection* output){
        inputs.resize(1);
        outputs.resize(1);
        outputs[0] = output;
    }

    void update() override{
        outputs[0]->setValue(inputs[0]->getVal());
    }

    void parse(string text) override{
        
    }

    static string type(){
        return "Output";
    }
};

namespace ComponentFactoryRegistrations{
    ComponentFactoryRegistration<Output> _Output(Output::type());
}

#endif