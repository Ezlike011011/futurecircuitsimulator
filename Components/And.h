#ifndef AND
#define AND

#include "../Circuit.h"

class And: public Circuit{
public:
    And(){
        inputs.resize(2);
        outputs.resize(1);
    }

    void update() override{
        outputs[0]->setValue(inputs[0]->getVal() && inputs[1]->getVal());
    }

    void parse(string text) override{
        
    }

    static string type(){
        return "And";
    }
};

namespace ComponentFactoryRegistrations{
    ComponentFactoryRegistration<And> _And(And::type());
}


#endif