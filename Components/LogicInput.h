#ifndef LOGICINPUT
#define LOGICINPUT

#include "../Circuit.h"
#include <iostream>

using std::cout;
using std::cin;
using std::endl;

class LogicInput: public Circuit{
private:
    bool value;
public:
    LogicInput(){
        outputs.resize(1);
    }

    void update() override{
        outputs[0]->setValue(value);
    }

    void prompt(){
        char a;
        cout << "Set probe: ";
        cin >> a;
        value = (a == 'T');
    }

    void setVal(bool val){
        value = val;
    }

    void parse(string text) override{
        value = (text=="1");
    }

    static string type(){
        return "Logic Input";
    }
};

namespace ComponentFactoryRegistrations{
    ComponentFactoryRegistration<LogicInput> _LogicInput(LogicInput::type());
}


#endif