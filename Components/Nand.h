#ifndef NAND
#define NAND

#include "../Circuit.h"

class Nand: public Circuit{
public:
    Nand(){
        inputs.resize(2);
        outputs.resize(1);
    }

    void update() override{
        outputs[0]->setValue(!(inputs[0]->getVal() && inputs[1]->getVal()));
    }

    void parse(string text) override{
        
    }

    static string type(){
        return "Nand";
    }
};

namespace ComponentFactoryRegistrations{
    ComponentFactoryRegistration<Nand> _Nand("Nand");
}

#endif