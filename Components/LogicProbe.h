#ifndef LOGICPROBE
#define LOGICPROBE

#include "../Circuit.h"
#include <iostream>

using std::cout;
using std::cin;
using std::endl;

class LogicProbe: public Circuit{
public:
    LogicProbe(){
        inputs.resize(1);
    }

    void update() override{
        cout << (inputs[0]->getVal()?"True": "False") << endl;
    }

    void parse(string text) override{
        
    }

    static string type(){
        return "Logic Probe";
    }
};

namespace ComponentFactoryRegistrations{
    ComponentFactoryRegistration<LogicProbe> _LogicProbe(LogicProbe::type());
}

#endif