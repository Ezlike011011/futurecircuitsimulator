#ifndef INPUT
#define INPUT

#include "../Circuit.h"

class Input: public Circuit{
public:
    Input(){
        inputs.resize(1);
        outputs.resize(1);
    }

    Input(Mesh* input){
        inputs.resize(1);
        outputs.resize(1);
        inputs[0] = input;
    }

    void update() override{
        outputs[0]->setValue(inputs[0]->getVal());
    }

    void parse(string text) override{
        
    }

    static string type(){
        return "Input";
    }
};

namespace ComponentFactoryRegistrations{
    ComponentFactoryRegistration<Input> _Input(Input::type());
}

#endif