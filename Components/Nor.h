#ifndef NOR
#define NOR

#include "../Circuit.h"

class Nor: public Circuit{
public:
    Nor(){
        inputs.resize(2);
        outputs.resize(1);
    }

    void update() override{
        outputs[0]->setValue(!(inputs[0]->getVal() || inputs[1]->getVal()));
    }

    void parse(string text) override{
        
    }

    static string type(){
        return "Nor";
    }
};

namespace ComponentFactoryRegistrations{
    ComponentFactoryRegistration<Nor> _Nor(Nor::type());
}


#endif