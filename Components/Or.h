#ifndef OR
#define OR

#include "../Circuit.h"

class Or: public Circuit{
public:
    Or(){
        inputs.resize(2);
        outputs.resize(1);
    }

    void update() override{
        outputs[0]->setValue(inputs[0]->getVal() || inputs[1]->getVal());
    }

    void parse(string text) override{
        
    }

    static string type(){
        return "Or";
    }
};

namespace ComponentFactoryRegistrations{
    ComponentFactoryRegistration<Or> _Or(Or::type());
}

#endif