#ifndef NOT
#define NOT

#include "../Circuit.h"

class Not: public Circuit{
public:
    Not(){
        inputs.resize(1);
        outputs.resize(1);
    }

    void update() override{
        outputs[0]->setValue(!inputs[0]->getVal());
    }

    void parse(string text) override{
        
    }

    static string type(){
        return "Not";
    }
};

namespace ComponentFactoryRegistrations{
    ComponentFactoryRegistration<Not> _Not(Not::type());
}

#endif