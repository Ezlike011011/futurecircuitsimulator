#ifndef CIRCUIT
#define CIRCUIT

#include "ComponentFactoryRegistration.h"
#include "Mesh.h"
#include "Connection.h"
#include <vector>
#include <set>
#include <string>
#include <map>
#include <utility>

using std::pair;
using std::map;
using std::string;
using std::set;
using std::vector;

enum Rotation{
    UP, RIGHT, DOWN, LEFT
};

class Circuit{
private:
    set<Circuit*> updateList;
    map<string, Circuit*> subCircuits;
    vector<Mesh*> meshes;

protected:
    pair<int, int> position;
    Rotation rotation;

    vector<Mesh*> inputs;
    vector<Connection*> outputs;
    
public:
    Circuit();
    ~Circuit();
    Circuit(int inputCount, int outputCount);

    //By default, update the contained components, override for individual components' operation
    virtual void update();

    void setRotation(Rotation r);
    void setPosition(int x, int y);

    void connectInputMesh(Mesh* inputMesh, int index);
    void connectOutputMesh(Mesh* outputMesh, int index);

    void addSubCircuit(Circuit* subCircuit, string name);
    Circuit* getSubCircuit(string name);
    void insertToUpdateList(Circuit* toUpdate);

    bool isStable();
    static string type();

    virtual void parse(string text);
    // virtual string format();
};

namespace ComponentFactoryRegistrations{
    ComponentFactoryRegistration<Circuit> _Circuit(Circuit::type());
}

#include "Circuit.hpp"

#endif