#include "FutureCircuitSimulator.h"

#include <iostream>

using std::cout;
using std::cin;
using std::endl;

int main(int argc, char const *argv[]){
    FutureCircuitSimulator fcs;
    fcs.parseFile("xor.fcs");

    Circuit* c = fcs.getCircuit();

    LogicInput* A = dynamic_cast<LogicInput*>(c->getSubCircuit("A"));        
    LogicInput* B = dynamic_cast<LogicInput*>(c->getSubCircuit("B"));
    
    for(int i = 0; i < 2; i++){
        for(int j = 0; j < 2; j++){
            A->setVal(i);
            B->setVal(j);

            c->insertToUpdateList(A);
            c->insertToUpdateList(B);

            while(!c->isStable()){
                c->update();
            }
        }
    }

    return 0;
}