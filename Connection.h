#ifndef CONNECTION
#define CONNECTION

#include "Mesh.h"
#include <iostream>

using std::cout;
using std::cin;
using std::endl;

class Mesh;

class Connection{
private:
    Mesh* tiePoint;
    bool value;
    bool previousValue;
public:
    Connection();
    Connection(Mesh* tp);
    Connection(Mesh* tp, bool val);

    Mesh* getTiePoint();
    void setTiePoint(Mesh* m);
    bool getValue();
    void setValue(const bool newValue);

    bool valueChanged();

    void print();
};

#include "Connection.hpp"

#endif