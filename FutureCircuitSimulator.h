#ifndef FUTURECIRCUITSIMULATOR
#define FUTURECIRCUITSIMULATOR

#include "ComponentFactory.h"
#include "Circuit.h"
#include "./Components/fundamentals.h"
#include "./Components/Io.h"

#include <string>
#include <fstream>

using std::stoi;
using std::map;
using std::ifstream;
using std::ofstream;
using std::string;

class FutureCircuitSimulator{
private:
    Circuit* operatingCircuit;
public:
    FutureCircuitSimulator();
    ~FutureCircuitSimulator();
    Circuit* parseFile(string fileName);
    void setCircuit(Circuit* c);
    Circuit* getCircuit();
};


#include "FutureCircuitSimulator.hpp"
#endif