#include "FutureCircuitSimulator.h"

FutureCircuitSimulator::FutureCircuitSimulator(){
    operatingCircuit = new Circuit();
}

FutureCircuitSimulator::~FutureCircuitSimulator(){
    delete operatingCircuit;
}

Circuit* FutureCircuitSimulator::parseFile(string fileName){
    ifstream ifs;
    ifs.open(fileName);

    /*
     * TODO: pre-processing
     *  check abstract circuit/eoc parity
     */


    //Reset file stream pointer and grab all of the text out of it
    ifs.seekg(0);
    string text((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());

    //Delete the operating circuit and any sub-circuits it may have had
    delete operatingCircuit;

    //Make a new operating circuit and populate it
    operatingCircuit = new Circuit();
    operatingCircuit->parse(text);
    return operatingCircuit;
}

void FutureCircuitSimulator::setCircuit(Circuit* c){
    operatingCircuit = c;
}

Circuit* FutureCircuitSimulator::getCircuit(){
    return operatingCircuit;
}