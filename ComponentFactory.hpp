#include "ComponentFactory.h"

ComponentFactory::ComponentFactory(){

}

ComponentFactory::ComponentFactory(const ComponentFactory& other){
    generatorMap = other.generatorMap;
}

map<string, CircuitInstanceGenerator> ComponentFactory::getGeneratorMap(){
    return generatorMap;
}

ComponentFactory::~ComponentFactory(){

}

ComponentFactory &ComponentFactory::get(){
    static ComponentFactory instance;
    return instance;
}

bool ComponentFactory::registerGenerator(const string name, const CircuitInstanceGenerator& func){
    return generatorMap.insert(make_pair(name,func)).second;
}

