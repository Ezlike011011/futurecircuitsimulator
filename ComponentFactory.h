#ifndef COMPONENTFACTORY
#define COMPONENTFACTORY

#include <string>
#include <map>
#include <utility>

using std::pair;
using std::map;
using std::string;

class Circuit;

typedef Circuit* (*CircuitInstanceGenerator)();


class ComponentFactory{
public:
    static ComponentFactory& get();
    map<string, CircuitInstanceGenerator> getGeneratorMap();
    bool registerGenerator(const string name, const CircuitInstanceGenerator& func);

private:
    ComponentFactory(const ComponentFactory& other);
    ComponentFactory();
    ~ComponentFactory();
    map<string, CircuitInstanceGenerator> generatorMap;

};

#include "ComponentFactory.hpp"
#endif