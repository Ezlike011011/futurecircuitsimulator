#include "Connection.h"

Connection::Connection(){

}

Connection::Connection(Mesh* tp){
    tiePoint = tp;
    value = false;
}

Connection::Connection(Mesh* tp, bool val){
    tiePoint = tp;
    value = val;
}

Mesh* Connection::getTiePoint(){
    return tiePoint;
}

void Connection::setTiePoint(Mesh* m){
    tiePoint = m;
}

bool Connection::getValue(){
    return value;
}

void Connection::setValue(const bool newValue){
    previousValue = value;
    value = newValue;
}

bool Connection::valueChanged(){
    return (previousValue != value);
}

void Connection::print(){
    cout << tiePoint << ", " << value << endl;
}