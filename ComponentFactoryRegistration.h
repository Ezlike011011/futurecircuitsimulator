#ifndef COMPONENTFACTORYREGISTRATION
#define COMPONENTFACTORYREGISTRATION

#include "ComponentFactory.h"

namespace ComponentFactoryRegistrations{
    template <typename T>
    class ComponentFactoryRegistration{
        public:
        ComponentFactoryRegistration(const string id){
            ComponentFactory::get().registerGenerator(id, [](){return static_cast<Circuit*>(new T());});
        }
    };
}

#endif